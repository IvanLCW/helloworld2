//
//  HelloViewController.swift
//  HelloWorld
//
//  Created by New Owner on 28/06/2016.
//  Copyright © 2016 gi. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet var hiButton: UIButton!
    @IBOutlet var byeButton: UIButton!
    @IBOutlet weak var changePage: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hiButton.addTarget(self, action: #selector(HelloViewController.sayHi), forControlEvents: UIControlEvents.TouchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sayHi(sender: AnyObject) {
        
        if let button: UIButton = sender as? UIButton {
            
            UIView.animateWithDuration(0.5, animations: {
            if button == self.hiButton {
                self.helloLabel.text = "😘"
                self.helloLabel.transform = CGAffineTransformIdentity
            }
            else if button == self.byeButton {
                self.helloLabel.text = "😟"
                self.helloLabel.transform = CGAffineTransformRotate(CGAffineTransformIdentity, CGFloat(-180.0*M_PI/180.0))
            }
        })
        }

    }
    
    @IBAction func sayBye() {
        helloLabel.text = "bye bye 😄"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
